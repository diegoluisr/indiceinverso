package co.edu.uniquindio.util;

import java.io.*;

public class LocalFileReader {

    public static String readFile(String location) {
        String result = "";
        BufferedReader reader;
        try {
            InputStream input = LocalFileReader.class.getClassLoader().getResourceAsStream(location);
            reader = new BufferedReader(new InputStreamReader(input));
            String line = reader.readLine();
            while (line != null) {
                line = reader.readLine();
                result += " " + line;
            }
            reader.close();
        } catch (IOException e) {
            e.printStackTrace();
        }

        return result;
    }
}
