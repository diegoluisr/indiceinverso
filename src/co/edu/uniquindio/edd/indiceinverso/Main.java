package co.edu.uniquindio.edd.indiceinverso;

import co.edu.uniquindio.util.LocalFileReader;

import java.util.HashMap;

public class Main {

    public Main() {

    }

    public static void main(String args[]) {
        String[] songs = new String[] {"song0.txt", "song1.txt", "song2.txt"};
        HashMap<String, String> songList = new HashMap<>();

        for (String song : songs) {
            String lyric = LocalFileReader.readFile("resources/" + song);
            songList.put(song, lyric);
        }

        RevertedIndex revertedIndex = new RevertedIndex(songList);
        System.out.println(revertedIndex);
    }
}
