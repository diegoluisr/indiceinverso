package co.edu.uniquindio.edd.indiceinverso;

import java.util.*;

public class RevertedIndex {

    private ArrayList<String> items;
    private LinkedHashMap<String, ArrayList<String>> inverted;

    public RevertedIndex() {
        this.inverted = new LinkedHashMap<>();
        this.items = new ArrayList<>();
    }

    public RevertedIndex(HashMap<String, String> items) {
        this.inverted = new LinkedHashMap<>();
        this.items = new ArrayList<>();

        for (Map.Entry<String, String> item: items.entrySet()) {
            addItem(item.getKey(), item.getValue());
        }
    }

    private ArrayList<String> getWords(String paragraph) {
        String[] words = paragraph.split(" ");
        return new ArrayList<>(Arrays.asList(words));
    }

    public void addItem(String key, String value) {
        ArrayList<String> words = getWords(value);
        for (String word: words) {
            if (inverted.containsKey(word)) {
                inverted.get(word).add(key);
            }
            else {
                inverted.put(word, new ArrayList<>());
            }
        }
    }

    public LinkedHashMap<String, ArrayList<String>> getReverted() {
        return getReverted();
    }
}
