Las palabras fueron avispas
Y las calles como dunas
Cuando aún te espero llegar
En un ataúd guardo tu tacto y una corona
Con tu pelo enmarañado
Queriendo encontrar un arcoíris
Infinito
Mis manos que aún son de hueso
Y tu vientre sabe a pan
La catedral es tu cuerpo
Eras verano y mil tormentas
Yo, el león
Que sonríe a las paredes que he vuelto a pintar del mismo
Color
No sé distinguir entre besos y raíces
No sé distinguir lo complicado de lo simple
Y ahora estás en mi lista
De promesas a olvidar
Todo arde si le aplicas la chispa adecuada
El fuego que era a veces propio
La ceniza siempre ajena
Blanca esperma resbalando
Por la espina dorsal
Ya somos más viejos y sinceros, ¿y qué más da?
Si miramos "la laguna", como llaman a la eternidad
De la ausencia
No sé distinguir entre besos y raíces
No sé distinguir lo complicado de lo simple
Y ahora estás en mi lista
De promesas a olvidar
Todo arde si le aplicas la chispa adecuada
La chispa adecuada
La chispa adecuada
La chispa adecuada
